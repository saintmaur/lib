package utils

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
	"unicode"
)

func IsTimeDuration(pattern string) (time.Duration, error) {
	d, err := time.ParseDuration(pattern)
	return d, err
}

func CreateDir(p string) error {
	if err := os.MkdirAll(filepath.Dir(p), 0777); err != nil {
		return err
	}
	return nil
}

func PrintVersion(appName, version, commit, buildTime string) {
	fmt.Printf("%s build info:\n  Version:\t%s\n  Commit:\t%s\n  Build time:\t%s\n", appName, version, commit, buildTime)
}

// PrepareDTFormat converts abstract format string into Go's one
func PrepareDTFormat(input string) string {
	formatMap := map[string]string{
		"yyyy": "2006",
		"mm":   "01",
		"mon":  "Jan",
		"dd":   "02",
		"hh":   "15",
		"MM":   "04",
		"ss":   "05",
		"ms":   "000",
		"us":   "000000",
		"ns":   "000000000",
		"AM":   "PM",
		"tmz":  "-0700",
	}
	output := input
	for key, value := range formatMap {
		output = strings.ReplaceAll(output, key, value)
	}
	return output
}

// MakeTimestamp creates a timestamp from nanoseconds
func MakeTimestamp(ns int64) time.Time {
	sec := ns / int64(time.Second)
	tm := time.Unix(sec, ns%sec)
	return tm
}

func GoodTemplateFunctionName(name string) bool {
	if name == "" {
		return false
	}
	for i, r := range name {
		switch {
		case r == '_':
		case i == 0 && !unicode.IsLetter(r):
			return false
		case !unicode.IsLetter(r) && !unicode.IsDigit(r):
			return false
		}
	}
	return true
}

func ParseDuration(tm string, def time.Duration) time.Duration {
	d, err := time.ParseDuration(tm)
	if err != nil {
		d = def
	}
	return d
}
