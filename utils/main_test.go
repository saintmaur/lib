package utils

import "testing"

func TestUtils_GoDateFormat(t *testing.T) {
	format := "dd.mm.yyyy---hh:MM:ss.ms"
	expected := "02.01.2006---15:04:05.000"
	actual := PrepareDTFormat(format)
	if actual != expected {
		t.Fatalf("Formats are distinct:\n\tactual: %v\n\texpected: %v", actual, expected)
	}
	format = "dd.mm.yyyy---hh:MM:ss.us"
	expected = "02.01.2006---15:04:05.000000"
	actual = PrepareDTFormat(format)
	if actual != expected {
		t.Fatalf("Formats are distinct:\n\tactual: %v\n\texpected: %v", actual, expected)
	}
	format = "dd.mm.yyyy---hh:MM:ss.ns"
	expected = "02.01.2006---15:04:05.000000000"
	actual = PrepareDTFormat(format)
	if actual != expected {
		t.Fatalf("Formats are distinct:\n\tactual: %v\n\texpected: %v", actual, expected)
	}
}
func TestUtils_GoodTemplateFunctionName(t *testing.T) {
	names := []string{"123", " message", "/sha2563", "^name"}
	for _, name := range names {
		if GoodTemplateFunctionName(name) {
			t.Fatalf("Name checking has failed on '%s'", name)
		}
	}
	names = []string{"good", "well", "nice", "best"}
	for _, name := range names {
		if !GoodTemplateFunctionName(name) {
			t.Fatalf("Name checking has failed on '%s'", name)
		}
	}
}
