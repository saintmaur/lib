package http

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"os"
	"regexp"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/gorilla/websocket"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/saintmaur/lib/logger"
)

const (
	certFilename   = "cert.pem"
	caCertFilename = "ca_cert.pem"
	keyFilename    = "key.pem"
	caKeyFilename  = "ca_key.pem"
	addressPattern = "^(.*):([0-9]+)$"
	randomPort     = "127.0.0.1:0"
	selfName       = "servlex"
)

var (
	env = make(map[string]string)
)

type Server struct {
	srv      *http.Server
	mux      *http.ServeMux
	upgrader *websocket.Upgrader
	config   *ServerConfig
	stopChan chan bool
	started  chan struct{}
	wg       sync.WaitGroup
}

func verifyConfig(serverConfig *ServerConfig) error {
	logger.Debugf("Try to validate the config: %+v", *serverConfig)
	if serverConfig.TLS.Enabled {
		if !serverConfig.TLS.GenCert && (len(serverConfig.TLS.CertFile) == 0 || len(serverConfig.TLS.KeyFile) == 0) {
			return fmt.Errorf("no certificates params are provided")
		}
		serverConfig.TLS.Domain = processTemplate(serverConfig.TLS.Domain)
		if len(serverConfig.TLS.Domain) == 0 {
			return fmt.Errorf("domain name must be provided")
		}
	}
	re, err := regexp.Compile(addressPattern)
	if err != nil {
		return err
	}
	serverConfig.Listen = processTemplate(serverConfig.Listen)
	match := re.FindString(serverConfig.Listen)
	if len(match) == 0 {
		logger.Infof("listen parameter '%s' is not of '[host]:port' format. Set it to a random value", serverConfig.Listen)
		serverConfig.Listen = randomPort
	}
	return nil
}

func processTemplate(data string) string {
	tmpl := template.New("template")
	tmpl, err := tmpl.Parse(data)
	if err != nil {
		logger.Errorf("Failed on validating the config: %s", err)
		return data
	}
	var buff bytes.Buffer
	err = tmpl.Execute(&buff, env)
	if err != nil {
		logger.Errorf("Failed on validating the config: %s", err)
		return data
	}
	return buff.String()
}

func initEnv() {
	for _, envPair := range os.Environ() {
		parts := strings.Split(envPair, "=")
		env[parts[0]] = strings.Join(parts[1:], "=")
	}
}

func New(serverConfig *ServerConfig) *Server {
	initEnv()
	if err := verifyConfig(serverConfig); err != nil {
		logger.Errorf("Failed on validating the config: %s", err)
		return nil
	}
	srv := new(Server)
	srv.config = serverConfig
	srv.started = make(chan struct{})
	srv.mux = http.NewServeMux()
	srv.srv = &http.Server{
		Addr:    serverConfig.Listen,
		Handler: srv.mux,
	}
	if serverConfig.TLS.Enabled {
		if serverConfig.TLS.GenCert {
			err := genCertificate(serverConfig)
			if err != nil {
				logger.Errorf("Failed on generating a certificate: %s", err)
				return nil
			}
			serverConfig.TLS.CertFile = certFilename
			serverConfig.TLS.KeyFile = keyFilename
		}
		srv.srv.TLSConfig = getTLSConfig(serverConfig)
	}
	if len(srv.config.ProbeEndPoint) == 0 {
		srv.config.ProbeEndPoint = "/metrics/"
	}
	srv.upgrader = &websocket.Upgrader{}
	srv.AddHandler(srv.config.ProbeEndPoint, promhttp.Handler().ServeHTTP)
	return srv
}

func (s *Server) start() error {
	ln, err := net.Listen("tcp", s.config.Listen)
	if err != nil {
		return err
	}
	addr := ln.Addr().(*net.TCPAddr)
	s.config.Listen = addr.String()
	logger.Infof("Start listening on %s", s.config.Listen)
	s.started <- struct{}{}
	if s.config.TLS.Enabled {
		return s.srv.ServeTLS(ln, s.config.TLS.CertFile, s.config.TLS.KeyFile)
	} else {
		return s.srv.Serve(ln)
	}
}

func (s *Server) stop() error {
	return s.srv.Shutdown(context.Background())
}

// Run starts the HTTP server for metrics exporting
func (s *Server) Run() {
	s.stopChan = make(chan bool)
	s.wg.Add(1)
	go func() {
		if err := s.start(); err != http.ErrServerClosed {
			logger.Fatalf("Failed on serving the HTTP connections: %s", err)
		}
		s.wg.Done()
	}()
	<-s.started
	s.waitForStart()
	logger.Info("The HTTP server has started successfully")
	s.wg.Add(1)
	go func() {
		logger.Debug("Start listening the HTTP service channels")
		<-s.stopChan
		if err := s.stop(); err != nil {
			logger.Errorf("Failed to stop the HTTP server: %s", err)
		} else {
			logger.Info("The HTTP server has stopped gracefully")
		}
		s.wg.Done()
	}()
}

func (s *Server) waitForStart() {
	re, _ := regexp.Compile(addressPattern)
	proto := "http"
	if s.config.TLS.Enabled {
		proto = "https"
	}
	address := re.ReplaceAllString(s.config.Listen, fmt.Sprintf("%s://$1:$2%s", proto, s.config.ProbeEndPoint))
	tr := http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
	client := http.Client{Transport: &tr}
	req, err := http.NewRequest("GET", address, nil)
	if err != nil {
		logger.Errorf("waitForStart: failed on creating a new request: %s", err)
		os.Exit(1)
	}
	req.Close = true
	for {
		_, err := client.Do(req)
		if err != nil {
			logger.Warnf("%s", err)
			time.Sleep(time.Duration(time.Millisecond * 100))
		} else {
			return
		}
	}
}

// Stop the server
func (s *Server) Stop() {
	s.stopChan <- true
	s.wg.Wait()
}

// AddHandler registers an endpoint to handle
func (srv *Server) AddHandler(endpoint string, f func(http.ResponseWriter, *http.Request)) {
	logger.Infof("Register a handler for '%s'", endpoint)
	srv.mux.HandleFunc(endpoint, f)
}
func (srv *Server) AddWSHandler(endpoint string, f func(msg []byte, mt int, c *websocket.Conn)) {
	logger.Infof("Register a WS handler for '%s'", endpoint)
	h := func(w http.ResponseWriter, r *http.Request) {
		logger.Debug("Upgrade connection")
		c, err := srv.upgrader.Upgrade(w, r, nil)
		if err != nil {
			logger.Errorf("Upgrade error: %s", err)
			return
		}
		logger.Debug("Start reading messages")
		for {
			mt, message, err := c.ReadMessage()
			if err != nil {
				logger.Warnf("Read error: %s", err)
				break
			}
			logger.Debugf("Got a message: %s", message)
			f(message, mt, c)
		}
	}
	srv.mux.HandleFunc(endpoint, h)
}

func getTLSConfig(serverConfig *ServerConfig) *tls.Config {
	tlsConfig := &tls.Config{
		ServerName: serverConfig.TLS.Domain,
		MinVersion: tls.VersionTLS12,
		NextProtos: []string{"h2", "http/1.1"},
	}
	return tlsConfig
}
