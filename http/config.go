package http

type TLSConfig struct {
	Enabled  bool   `mapstructure:"enabled"`
	GenCert  bool   `mapstructure:"generate_certificate"`
	ACME     bool   `mapstructure:"acme"`
	Domain   string `mapstructure:"domain"`
	CertFile string `mapstructure:"cert_file"`
	KeyFile  string `mapstructure:"key_file"`
}

// ServerConfig is a type describing config for the web server
type ServerConfig struct {
	Listen        string    `mapstructure:"listen"`
	TLS           TLSConfig `mapstructure:"tls"`
	ProbeEndPoint string    `mapstructure:"probe_endpoint"`
}
