package http

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"net"
	"os"
	"time"

	"gitlab.com/saintmaur/lib/logger"
)

func genCertificate(cfg *ServerConfig) error {
	caTmpl, caKey, err := prepareCert(nil, caKeyFilename, caCertFilename, nil, nil, true)
	if err != nil {
		return err
	}
	_, _, err = prepareCert(cfg, keyFilename, certFilename, caKey, caTmpl, true)
	return err
}

func prepareCert(cfg *ServerConfig,
	keyFilename, certFilename string,
	caKey *ecdsa.PrivateKey, caCert *x509.Certificate, isCA bool) (*x509.Certificate, *ecdsa.PrivateKey, error) {
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		logger.Errorf("Failed to generate serial number: %v", err)
		return nil, nil, err
	}
	tmpl := &x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{selfName},
			CommonName:   selfName,
		},
		NotBefore:   time.Now(),
		NotAfter:    time.Now().Add(10000 * time.Hour),
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		IsCA:        isCA,
	}
	if cfg != nil {
		domain := net.ParseIP(cfg.TLS.Domain)
		if domain != nil {
			tmpl.IPAddresses = []net.IP{net.ParseIP(cfg.TLS.Domain), net.IPv4(127, 0, 0, 1), net.IPv6loopback}
		} else {
			tmpl.DNSNames = []string{cfg.TLS.Domain}
		}
	}
	privateKey, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		return nil, nil, err
	}
	if caCert == nil {
		caCert = tmpl
	}
	if caKey == nil {
		caKey = privateKey
	}
	caBytes, err := x509.CreateCertificate(rand.Reader, tmpl, caCert, &privateKey.PublicKey, caKey)
	if err != nil {
		return nil, nil, err
	}
	cert := new(bytes.Buffer)
	err = pem.Encode(cert, &pem.Block{Type: "CERTIFICATE", Bytes: caBytes})
	if err != nil {
		logger.Errorf("Failed to encode certificate to PEM: %s", err)
		return nil, nil, err
	}
	if err := os.WriteFile(certFilename, cert.Bytes(), 0644); err != nil {
		logger.Errorf("Failed to write the cert: %s", err)
		return nil, nil, err
	}
	logger.Infof("Prepared the certificate file: %s", certFilename)
	keyBytes, err := x509.MarshalPKCS8PrivateKey(privateKey)
	if err != nil {
		logger.Errorf("Unable to marshal private key: %v", err)
		return nil, nil, err
	}
	key := new(bytes.Buffer)
	err = pem.Encode(key, &pem.Block{Type: "PRIVATE KEY", Bytes: keyBytes})
	if err != nil {
		logger.Errorf("Failed to encode key to PEM: %s", err)
		return nil, nil, err
	}
	if err = os.WriteFile(keyFilename, key.Bytes(), 0600); err != nil {
		logger.Errorf("Failed to write the key: %s", err)
		return nil, nil, err
	}
	logger.Infof("Prepared the key file: %s", keyFilename)
	return tmpl, privateKey, nil
}
