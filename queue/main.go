package queue

import (
	"sync"
)

type Queue[T interface{}] struct {
	_q      []T
	dataC   chan T
	blocked bool
	mu      sync.Mutex
}

func NewQueue[T interface{}]() *Queue[T] {
	b := &Queue[T]{
		_q:    []T{},
		dataC: make(chan T, 1),
	}
	return b
}

func (q *Queue[T]) Data() <-chan T {
	return q.dataC
}

func (q *Queue[T]) Len() int {
	q.mu.Lock()
	defer q.mu.Unlock()
	return len(q._q)
}

func (q *Queue[T]) Push(value T) {
	q.mu.Lock()
	defer q.mu.Unlock()
	q._q = append(q._q, value)
	q.produce()
}

func (q *Queue[T]) produce() {
	if !q.blocked && len(q.dataC) == 0 && len(q._q) > 0 {
		value := q._q[0]
		q.blocked = true
		q.dataC <- value
	}
}

func (q *Queue[T]) Nack() {
	q.mu.Lock()
	defer q.mu.Unlock()
	q.blocked = false
	q.produce()
}

func (q *Queue[T]) Ack() {
	q.mu.Lock()
	defer q.mu.Unlock()
	if !q.blocked || len(q.dataC) > 0 {
		return
	}
	switch len(q._q) {
	case 1:
		q._q = q._q[:0]
	default:
		q._q = q._q[1:]
	}
	q.blocked = false
	q.produce()
}
