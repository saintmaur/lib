package queue

import (
	"fmt"
	"strings"
	"testing"
)

const size int = 10

func create_queue(t *testing.T, size int) *Queue[int] {
	q := NewQueue[int]()
	if q == nil {
		t.Fatal("Failed to create a queue")
	}
	return q
}
func Test_Create(t *testing.T) {
	create_queue(t, size)
}
func Test_Ack(t *testing.T) {
	expected := ""
	finishedC := make(chan struct{})
	fatalC := make(chan string)
	q := create_queue(t, size)
	for i := 0; i < size; i++ {
		expected += fmt.Sprintf("%d", i)
		value := i
		q.Push(value)
	}
	if q.Len() != size {
		t.Fatal("Wrong queue size")
	}
	q.Ack()
	if q.Len() != size {
		t.Fatal("Ack should not work")
	}
	actual := ""
	go func() {
		i := 0
		for {
			value := <-q.Data()
			actual += fmt.Sprintf("%d", value)
			q.Ack()
			qlen := q.Len()
			q.Ack()
			if q.Len() != qlen {
				fatalC <- "Ack should not work"
			}
			i++
			if i == 10 {
				finishedC <- struct{}{}
			}
		}
	}()
	select {
	case msg := <-fatalC:
		t.Fatal(msg)
	case <-finishedC:
		fmt.Println("Finished")
	}
	if q.Len() != 0 {
		t.Fatal("Queue must be empty!")
	}
	q.Ack()
	if actual != expected {
		t.Fatalf("Received data is incorrect:\n\texpected: %s\n\tactual: %s", expected, actual)
	}
}
func Test_Ack_Multithread(t *testing.T) {
	expected := ""
	finishedC := make(chan struct{})
	stopC := make(chan struct{})
	q := create_queue(t, size)
	actual := ""
	go func() {
		i := 0
		for {
			value := <-q.Data()
			actual += fmt.Sprintf("%d", value)
			q.Ack()
			i++
			if i == size {
				finishedC <- struct{}{}
			}
		}
	}()
	go func() {
		for i := 0; i < size; i++ {
			expected += fmt.Sprintf("%d", i)
			value := i
			q.Push(value)
		}
		<-finishedC
		fmt.Println("Finished")
		stopC <- struct{}{}
	}()
	<-stopC
	fmt.Println("Stopped")
	if q.Len() != 0 {
		t.Fatal("Queue must be empty!")
	}
	if actual != expected {
		t.Fatalf("Received data is incorrect:\n\texpected: %s\n\tactual: %s", expected, actual)
	}
}

func Test_Nack(t *testing.T) {
	expected := strings.Repeat("0", size)
	q := create_queue(t, size)
	for i := 0; i < size; i++ {
		value := i
		q.Push(value)
	}
	if q.Len() != size {
		t.Fatal("Wrong queue size")
	}
	var value int
	final := ""
	for i := 0; i < size; i++ {
		value = <-q.Data()
		final += fmt.Sprintf("%d", value)
		q.Nack()
	}
	if final != expected {
		t.Fatalf("Received data is incorrect:\n\texpected: %s\n\tactual: %s", expected, final)
	}
}
func Test_NackAck(t *testing.T) {
	expected := strings.Repeat("0", size)
	q := create_queue(t, size)
	for i := 0; i < size; i++ {
		value := i
		q.Push(value)
	}
	if q.Len() != size {
		t.Fatal("Wrong queue size")
	}
	var value int
	final := ""
	for i := 0; i < size; i++ {
		value = <-q.Data()
		final += fmt.Sprintf("%d", value)
		q.Nack()
		q.Ack()
	}
	if final != expected {
		t.Fatalf("Received data is incorrect:\n\texpected: %s\n\tactual: %s", expected, final)
	}
}
