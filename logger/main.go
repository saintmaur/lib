package logger

import (
	"fmt"

	// log "github.com/sirupsen/logrus"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Logger struct {
	l *zap.Logger
}

var (
	stopC chan struct{}
	lggr  *Logger
)

func setLevel(level string) zapcore.Level {
	var logLevel zapcore.Level
	switch level {
	case "debug":
		logLevel = zapcore.DebugLevel
	case "info":
		logLevel = zapcore.InfoLevel
	case "warn":
		logLevel = zapcore.WarnLevel
	case "error":
		logLevel = zapcore.ErrorLevel
	case "fatal":
		logLevel = zapcore.FatalLevel
	case "panic":
		logLevel = zapcore.PanicLevel
	default:
		logLevel = zapcore.InfoLevel
	}
	return logLevel
}

func InitLogger(fileName string, level string, formatJSON bool) bool {
	stopC = make(chan struct{})
	var err error
	encoding := "console"
	if formatJSON {
		encoding = "json"
	}
	if len(fileName) == 0 {
		fileName = "stdout"
	}
	cfg := zap.Config{
		Level:       zap.NewAtomicLevelAt(setLevel(level)),
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:      encoding,
		DisableCaller: true,
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "ts",
			LevelKey:       "level",
			NameKey:        "logger",
			FunctionKey:    zapcore.OmitKey,
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.RFC3339NanoTimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
		OutputPaths:      []string{fileName},
		ErrorOutputPaths: []string{fileName},
	}
	l, err := cfg.Build()
	if err != nil {
		fmt.Printf("Failed on creating a logger: %s\n", err)
		return false
	}
	lggr = &Logger{
		l: l,
	}
	go func() {
		<-stopC
		lggr.l.Sync()
	}()
	return true
}

func Stop() {
	stopC <- struct{}{}
}

func (lggr *Logger) WithField(key string, value string) *Logger {
	copy := *lggr
	copy.l = lggr.l.With(zap.Field{
		Key:    key,
		Type:   zapcore.StringType,
		String: value,
	})
	return &copy
}

func WithField(key string, value string) *Logger {
	return lggr.WithField(key, value)
}

func (lggr *Logger) Debug(a string) {
	lggr.l.Debug(a)
}
func (lggr *Logger) Info(a string) {
	lggr.l.Info(a)
}
func (lggr *Logger) Warn(a string) {
	lggr.l.Warn(a)
}
func (lggr *Logger) Error(a string) {
	lggr.l.Error(a)
}
func (lggr *Logger) Fatal(a string) {
	lggr.l.Fatal(a)
}
func (lggr *Logger) Panic(a string) {
	lggr.l.Panic(a)
}

func (lggr *Logger) Debugf(f string, a ...interface{}) {
	lggr.l.Sugar().Debugf(f, a...)
}
func (lggr *Logger) Infof(f string, a ...interface{}) {
	lggr.l.Sugar().Infof(f, a...)
}
func (lggr *Logger) Warnf(f string, a ...interface{}) {
	lggr.l.Sugar().Warnf(f, a...)
}
func (lggr *Logger) Errorf(f string, a ...interface{}) {
	lggr.l.Sugar().Errorf(f, a...)
}
func (lggr *Logger) Fatalf(f string, a ...interface{}) {
	lggr.l.Sugar().Fatalf(f, a...)
}
func (lggr *Logger) Panicf(f string, a ...interface{}) {
	lggr.l.Sugar().Panicf(f, a...)
}
func Debug(a string) {
	lggr.Debug(a)
}
func Info(a string) {
	lggr.Info(a)
}
func Warn(a string) {
	lggr.Warn(a)
}
func Error(a string) {
	lggr.Error(a)
}
func Fatal(a string) {
	lggr.Fatal(a)
}
func Panic(a string) {
	lggr.Panic(a)
}

func Debugf(f string, a ...interface{}) {
	lggr.l.Sugar().Debugf(f, a...)
}
func Infof(f string, a ...interface{}) {
	lggr.l.Sugar().Infof(f, a...)
}
func Warnf(f string, a ...interface{}) {
	lggr.l.Sugar().Warnf(f, a...)
}
func Errorf(f string, a ...interface{}) {
	lggr.l.Sugar().Errorf(f, a...)
}
func Fatalf(f string, a ...interface{}) {
	lggr.l.Sugar().Fatalf(f, a...)
}
func Panicf(f string, a ...interface{}) {
	lggr.l.Sugar().Panicf(f, a...)
}
