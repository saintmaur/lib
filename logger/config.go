package logger

// LoggerConfig is nothing more than a logger config type
type LoggerConfig struct {
	Filename   string `mapstructure:"filename"`
	Level      string `mapstructure:"level"`
	FormatJSON bool   `mapstructure:"json"`
}
