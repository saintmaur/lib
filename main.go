package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/profiler"
	"gitlab.com/saintmaur/lib/stat"
)

func main() {
	for _, cfg := range []profiler.ProfilerConfig{
		{
			Directory: "/tmp/",
			Type:      "mem",
		},
	} {
		prflr := new(profiler.Profiler)
		prflr.Start(cfg)
		defer prflr.Stop()
	}
	defer logger.Stop()
	if !logger.InitLogger("", "debug", false) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	srv := httpex.New(&httpex.ServerConfig{
		Listen: ":{{.SERVER_PORT}}",
		TLS: httpex.TLSConfig{
			Enabled: true,
			GenCert: true,
			Domain:  "{{.SERVER_DOMAIN}}",
		},
	})
	if srv == nil {
		logger.Info("Failed on creating a metrics server. Stopping")
		stat.Stop()
		os.Exit(1)
	}
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	logger.Info("All configurable executors have been started.")
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	logger.Info("A signal has been received. Stopping.")
	start := time.Now()
	stat.Stop()
	srv.Stop()
	logger.Infof("Stopped for %s", time.Since(start))
}
