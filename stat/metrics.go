package stat

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/saintmaur/lib/logger"
)

var (
	metricsRegistry = make(map[string]*prometheus.GaugeVec)
)

// Rec wraps one metric stat
type Rec struct {
	Name        string
	Value       float64
	Labels      []string
	LabelValues []string
}

func doStat(metric Rec) {
	logger.Debugf("A new stat has come: '%+v'", metric)
	name := metric.Name
	defer func() {
		if r := recover(); r != nil {
			logger.Warnf("Recovered on stat: %s", r)
		}
	}()
	registerMetric(name, metric.Labels)
	m := getMetric(name)
	if m != nil {
		(*m).WithLabelValues(metric.LabelValues...).Set(metric.Value)
	} else {
		logger.Warn("Impossible situation: no such metric after registering. o_O")
	}
}

func registerMetric(name string, labels []string) {
	if _, present := metricsRegistry[name]; present {
		return
	}
	metric := prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: name,
	}, labels)
	prometheus.MustRegister(metric)
	metricsRegistry[name] = metric
}

func getMetric(name string) *prometheus.GaugeVec {
	if m, present := metricsRegistry[name]; !present {
		return nil
	} else {
		return m
	}
}
