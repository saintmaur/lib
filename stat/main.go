package stat

import (
	"sync"

	"gitlab.com/saintmaur/lib/logger"
)

// Chan is a type alias for outer usage
type Chan chan Rec

var (
	wg    sync.WaitGroup
	stopC chan bool
	StatC Chan
)

func init() {
	stopC = make(chan bool)
}

// Run starts the channel for metrics
func Run(statC Chan) {
	StatC = statC
	wg.Add(1)
	go func() {
		logger.Debug("Start listening the service channels")
		for {
			select {
			case <-stopC:
				wg.Done()
				return
			case newStat, ok := <-StatC:
				if ok {
					doStat(newStat)
				} else {
					wg.Done()
					return
				}
			}
		}
	}()
	logger.Info("The metrics channel listener has started successfully")
}

func Stop() {
	stopC <- true
	wg.Wait()
}
