VERSION := $(shell git describe --tags 2> /dev/null || echo no-tag)
BRANCH := $(shell git symbolic-ref -q --short HEAD)
COMMIT := $(shell git rev-parse HEAD)
APP_NAME := lib
# Go related variables.
INSTALL_DIR ?= $(shell uname)
GO_VERSION := $(shell go version | col )
BUILD_TIME := $(shell date '+%Y-%m-%d_%H:%M:%S_%Z')
# Use linker flags to provide version/build settings
LDFLAGS := -X main.version=$(VERSION) -X main.commit=$(COMMIT) -X main.buildTime=$(BUILD_TIME)

docker:
	@docker build --force-rm -t saintmaur/$(APP_NAME):$(VERSION) .
	@docker push saintmaur/$(APP_NAME):$(VERSION)

deps:
	@echo Build with \"`go version`\"
	@mkdir -p bin/$(INSTALL_DIR)
	@rm -f bin/$(INSTALL_DIR)/*
	@echo "Install the dependencies..."
	@time -p go mod download && time -p go mod tidy && time -p go mod vendor
	@echo "------------------"
	@echo "Inspect the code..."
	@time -p go vet ./...
	@echo "------------------"

build: deps
	@echo "Build '$(APP_NAME)'..."
	@time -p go build -ldflags "$(LDFLAGS) -X main.appName=$(APP_NAME)" $(GCFLAGS) -o bin/$(INSTALL_DIR)/$(APP_NAME) main.go
	@echo "------------------"

coverage:
	@export GOCACHE=off
	@go test -coverprofile=coverage.out ./...
	@go tool cover -html=coverage.out
test:
	@export GOCACHE=off
	@go test -cover ./...

bench:
	@go test -bench=. -benchmem -run=^$$ ./

race:
	@go test -race ./...