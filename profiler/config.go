package profiler

// LoggerConfig is nothing more than a logger config type
type ProfilerConfig struct {
	Directory string `mapstructure:"directory"`
	Type      string `mapstructure:"type"`
}
