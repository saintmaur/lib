package profiler

import (
	"github.com/pkg/profile"
)

const (
	cpu          = "cpu"
	mem          = "mem"
	mutex        = "mutex"
	block        = "block"
	trace        = "trace"
	threadCreate = "thread_create"
	goroutine    = "goroutine"
)

type Profiler struct {
	stopper interface{ Stop() }
}

func (p *Profiler) Start(cfg ProfilerConfig) {
	var tp func(*profile.Profile)
	switch cfg.Type {
	case cpu:
		tp = profile.CPUProfile
	case mem:
		tp = profile.MemProfile
	case mutex:
		tp = profile.MutexProfile
	case block:
		tp = profile.BlockProfile
	case trace:
		tp = profile.TraceProfile
	case threadCreate:
		tp = profile.ThreadcreationProfile
	case goroutine:
		tp = profile.GoroutineProfile
	default:
		tp = profile.CPUProfile
	}
	p.stopper = profile.Start(tp, profile.ProfilePath(cfg.Directory), profile.NoShutdownHook)
}
func (p *Profiler) Stop() {
	p.stopper.Stop()
}
