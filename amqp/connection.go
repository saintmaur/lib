package amqp

import (
	"fmt"
	"sync"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/saintmaur/lib/logger"
)

type Message struct {
	Delivery amqp.Delivery
}

type Channel struct {
	ch          *amqp.Channel
	q           amqp.Queue
	config      *AMQPConfig
	stopC       chan bool
	stopHandler func()
}

type Connection struct {
	conn         *amqp.Connection
	url          string
	consumers    map[string]*Channel
	producers    map[string]*Channel
	mu           sync.Mutex
	stopC        chan bool
	postStopHook func()
}

func newConnection(url string, stopC chan bool, postStopHook func()) *Connection {
	connection := new(Connection)
	connection.stopC = stopC
	connection.postStopHook = postStopHook
	connection.consumers = map[string]*Channel{}
	connection.producers = map[string]*Channel{}
	connection.url = url
	return connection
}

func (connection *Connection) keep() {
	go func() {
		for {
			select {
			case <-time.After(time.Second * 5):
				if connection.isClosed() {
					err := connection.reconnectIfNecessary()
					if err != nil {
						logger.Warnf("Failed to reconnect: %s", err)
					}
				}
			case <-connection.stopC:
				logger.Infof("Try to close the connection '%s'", connection.url)
				logger.Infof("Try to close the consumer channels (%d)", len(connection.consumers))
				for channelName := range connection.consumers {
					connection.closeConsumerChannel(channelName)
				}
				logger.Infof("Try to close the producer channels (%d)", len(connection.producers))
				for channelName := range connection.producers {
					connection.closeProducerChannel(channelName)
				}
				if connection.conn != nil && !connection.conn.IsClosed() {
					logger.Infof("...disconnecting from '%s'", connection.url)
					err := connection.conn.Close()
					if err != nil {
						logger.Infof("Got an error while disconnecting from '%s'", connection.url)
					}
					logger.Infof("Disconnected from '%s'", connection.url)
				}
				connection.postStopHook()
				return
			}
		}
	}()
}

func (connection *Connection) reconnectIfNecessary() error {
	connection.mu.Lock()
	defer connection.mu.Unlock()
	for _, channel := range connection.consumers {
		if !channel.ch.IsClosed() {
			channel.ch.Cancel(channel.config.Name, false)
			channel.ch.Close()
		}
	}
	for _, channel := range connection.producers {
		if !channel.ch.IsClosed() {
			channel.ch.Close()
		}
	}
	var err error
	connection.conn, err = initConnection(connection.url)
	if err != nil {
		return err
	}
	for _, channel := range connection.consumers {
		if channel.ch.IsClosed() {
			ch, err := connection.createChannel(channel.config)
			if err != nil {
				return err
			}
			connection.consumers[channel.config.Name].ch = ch
		}
	}
	return nil
}

func (connection *Connection) GetConsumerChannel(amqpConfig *AMQPConfig) (*Channel, error) {
	return connection.getChannel(amqpConfig, connection.consumers)
}

func (connection *Connection) GetProducerChannel(amqpConfig *AMQPConfig) (*Channel, error) {
	return connection.getChannel(amqpConfig, connection.producers)
}

func (connection *Connection) getChannel(amqpConfig *AMQPConfig, storage map[string]*Channel) (*Channel, error) {
	if connection.isClosed() {
		return nil, fmt.Errorf("not connected to AMQP server")
	}
	connection.mu.Lock()
	channel, ok := storage[amqpConfig.Name]
	connection.mu.Unlock()
	if ok {
		return channel, nil
	}
	ch := new(Channel)
	ch.config = amqpConfig
	var err error
	ch.ch, err = connection.createChannel(amqpConfig)
	if err != nil {
		return nil, err
	}
	connection.mu.Lock()
	storage[amqpConfig.Name] = ch
	connection.mu.Unlock()
	return ch, nil
}

func (connection *Connection) createChannel(amqpConfig *AMQPConfig) (*amqp.Channel, error) {
	if connection.isClosed() {
		return nil, fmt.Errorf("not connected to AMQP server")
	}
	ch, err := connection.conn.Channel()
	if err != nil {
		return nil, fmt.Errorf("failed to open a channel: %s", err)
	}
	err = ch.Qos(
		1,     // prefetch count
		0,     // prefetch size
		false, // global
	)
	if err != nil {
		return nil, fmt.Errorf("failed to set a Qos: %s", err)
	}
	//TODO: check if necessary by config
	err = ch.ExchangeDeclare(
		amqpConfig.Exchange.Name,
		amqpConfig.Exchange.Type,
		amqpConfig.Exchange.Durable,
		amqpConfig.Exchange.AutoDelete,
		amqpConfig.Exchange.Internal,
		amqpConfig.Exchange.NoWait,
		amqpConfig.Exchange.Args,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to declare an exchange: %s", err)
	}
	//TODO: check if necessary by config
	_, err = ch.QueueDeclare(
		amqpConfig.Queue.Name,
		amqpConfig.Queue.Durable,
		amqpConfig.Queue.AutoDelete,
		amqpConfig.Queue.Exclusive,
		amqpConfig.Queue.NoWait,
		amqpConfig.Queue.Args,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to declare a queue: %s", err)
	}
	//TODO: check if necessary by config
	err = ch.QueueBind(
		amqpConfig.Queue.Name,
		amqpConfig.Queue.RoutingKey,
		amqpConfig.Exchange.Name,
		false,
		nil,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to bind a queue: %s", err)
	}
	return ch, nil
}

func (connection *Connection) closeConsumerChannel(name string) {
	connection.mu.Lock()
	defer connection.mu.Unlock()
	channel, ok := connection.consumers[name]
	if !ok {
		return
	}
	if !channel.ch.IsClosed() {
		channel.ch.Cancel(channel.config.Name, false)
		channel.ch.Close()
	}
}

func (connection *Connection) closeProducerChannel(name string) {
	connection.mu.Lock()
	defer connection.mu.Unlock()
	channel, ok := connection.producers[name]
	if !ok {
		return
	}
	if !channel.ch.IsClosed() {
		channel.ch.Close()
	}
}

func initConnection(url string) (*amqp.Connection, error) {
	conn, err := amqp.Dial(url)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to RabbitMQ: %s", err)
	}
	return conn, nil
}

func (connection *Connection) isClosed() bool {
	return connection.conn == nil || connection.conn.IsClosed()
}
