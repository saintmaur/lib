package amqp

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

type QueueConfig struct {
	Name       string                 `mapstructure:"name"`
	Durable    bool                   `mapstructure:"durable"`
	RoutingKey string                 `mapstructure:"routing_key"`
	Args       map[string]interface{} `mapstructure:"args"`
	Passive    bool                   `mapstructure:"passive"`
	AutoDelete bool                   `mapstructure:"auto_delete"`
	Exclusive  bool                   `mapstructure:"exclusive"`
	NoWait     bool                   `mapstructure:"nowait"`
}

type ExchangeConfig struct {
	Name       string                 `mapstructure:"name"`
	Type       string                 `mapstructure:"type"`
	Passive    bool                   `mapstructure:"passive"`
	Durable    bool                   `mapstructure:"durable"`
	AutoDelete bool                   `mapstructure:"auto_delete"`
	Internal   bool                   `mapstructure:"internal"`
	NoWait     bool                   `mapstructure:"nowait"`
	Args       map[string]interface{} `mapstructure:"args"`
}
type MessageConfig struct {
	Headers         map[string]interface{} `mapstructure:"headers"`
	ContentType     string                 `mapstructure:"content_type"`
	DeliveryMode    uint8                  `mapstructure:"delivery_mode"`
	ContentEncoding string                 `mapstructure:"content_encoding"`
	Priority        uint8                  `mapstructure:"priority"`
	CorrelationId   string                 `mapstructure:"correlation_id"`
	ReplyTo         string                 `mapstructure:"reply_to"`
	Expiration      string                 `mapstructure:"expiration"`
	MessageId       string                 `mapstructure:"message_id"`
	Type            string                 `mapstructure:"type"`
	UserId          string                 `mapstructure:"user_id"`
	AppId           string                 `mapstructure:"app_id"`
}

// AMQPConfig is a type describing config for the AMQP consumer
type AMQPConfig struct {
	Name     string         `mapstructure:"name"`
	URL      string         `mapstructure:"url"`
	Queue    QueueConfig    `mapstructure:"queue"`
	Exchange ExchangeConfig `mapstructure:"exchange"`
	Message  MessageConfig  `mapstructure:"message"`
}

func ParseConfig(data string) (*AMQPConfig, error) {
	configParser := viper.New()
	config := new(AMQPConfig)
	configParser.SetConfigType("json")
	err := configParser.ReadConfig(strings.NewReader(data))
	if err != nil {
		return nil, fmt.Errorf("failed to read the config: %s", err)
	}
	err = configParser.Unmarshal(config)
	if err != nil {
		return nil, fmt.Errorf("failed to parse the config: %s", err)
	}
	return config, nil
}
