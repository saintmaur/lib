package amqp

import (
	"context"
	"fmt"
	"time"

	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/saintmaur/lib/logger"
)

func Connect(url string, stopC chan bool, postStopHook func()) (*Connection, error) {
	connection := newConnection(url, stopC, postStopHook)
	var err error
	connection.conn, err = initConnection(url)
	if err != nil {
		logger.Warnf("%s", err)
	}
	connection.keep()
	return connection, nil
}

func StartConsumer(connection *Connection, amqpConfig *AMQPConfig, handler func(Message), stopHandler func(), stopC chan bool, autoAck bool) error {
	if connection.isClosed() {
		return fmt.Errorf("not connected to AMQP server")
	}
	channel, err := connection.GetConsumerChannel(amqpConfig)
	if err != nil {
		return err
	}
	channel.stopC = stopC
	channel.stopHandler = stopHandler
	msgs, err := channel.ch.Consume(
		channel.q.Name,                // queue
		channel.config.Name,           // consumer
		!channel.config.Queue.Durable, // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		channel.config.Queue.Args,     // args
	)
	if err != nil {
		return fmt.Errorf("failed to register a consumer: %s", err)
	}
	go func() {
		defer channel.stopHandler()
		for {
			select {
			case <-channel.stopC:
				logger.Info("Stop the consumer")
				connection.closeConsumerChannel(channel.config.Name)
				logger.Infof("The consumer '%s' has been stopped and deleted", channel.config.Name)
				return
			case d := <-msgs:
				if channel.ch.IsClosed() {
					logger.Warnf("The channel '%s' is closed, exit", channel.config.Name)
					return
				}
				handler(Message{Delivery: d})
				if autoAck && channel.config.Queue.Durable {
					err := d.Ack(false)
					if err != nil {
						logger.Warnf("Failed to acknowledge: %s", err)
					}
				}
			}
		}
	}()
	return nil
}

func Publish(connection *Connection, amqpConfig *AMQPConfig, data string) error {
	if connection.isClosed() {
		return fmt.Errorf("not connected to AMQP server")
	}
	channel, err := connection.GetProducerChannel(amqpConfig)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	err = channel.ch.PublishWithContext(ctx,
		channel.config.Exchange.Name,    // exchange
		channel.config.Queue.RoutingKey, // routing key
		false,                           // mandatory
		false,                           // immediate
		amqp.Publishing{
			Headers:         channel.config.Message.Headers,
			DeliveryMode:    channel.config.Message.DeliveryMode,
			ContentType:     channel.config.Message.ContentType,
			ContentEncoding: channel.config.Message.ContentEncoding,
			Priority:        channel.config.Message.Priority,
			CorrelationId:   channel.config.Message.CorrelationId,
			ReplyTo:         channel.config.Message.ReplyTo,
			Expiration:      channel.config.Message.Expiration,
			MessageId:       channel.config.Message.MessageId,
			Type:            channel.config.Message.Type,
			UserId:          channel.config.Message.UserId,
			AppId:           channel.config.Message.AppId,
			Timestamp:       time.Now(),
			Body:            []byte(data),
		})
	return err
}
