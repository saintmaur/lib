# Configuration
To configure the connection provide a JSON-formatted string:
``` json
{
		"name": "test_channel",
		"url": "",
		"message": {
			"content_type": "text/plain"
		},
		"queue": {
			"name": "test_queue",
			"routing_key": "test_key"
		},
		"exchange": {
			"name": "test_exchange",
			"type": "direct",
			"auto_delete": false,
			"durable": true
		}
	}
{
  "name": "",
  "url": "amqp://guest:guest@localhost:5672/",
  "queue": {
    "passive": true,
    "auto_delete": true,
    "exclusive": true,
    "no_wait": true,
    "name": "",
    "durable": true,
    "routing_key": ""
  },
  "exchange": {
    "passive": true,
    "durable": true,
    "auto_delete": true,
    "internal": true,
    "no_wait": true,
    "name": "",
    "type": "direct"
  },
  "message": {
    "expiration": "",
    "message_id": "",
    "user_id": "",
    "app_id": "",
    "content_encoding": "",
    "reply_to": "",
    "type": "",
    "content_type": "",
    "correlation_id": ""
  }
}
```

# Usage example
## Connect to the server
``` go
func connect(amqpConfig string) (chan bool, error) {
  cfg, err := amqp.ParseConfig(amqpConfig)
  if err != nil {
    return nil, fmt.Errorf("Failed on parsing the configuration string: %s", err)
  }
  stopC := make(chan bool)
  connection, err := amqp.Connect(url, stopC, func() {
    // do something afterwords if necessary
  })
  if err != nil {
    return nil, fmt.Errorf("Failed on connecting to the server: %s", err)
  }
  return stopC, nil
}

```
## Publish a message
``` go
func publish(connection *Connection, cfg *AMQPConfig, data string) error {
  return amqp.Publish(connection, cfg, data)
}

```
## Consume messages
``` go
func startConsuming(connection *Connection, cfg *AMQPConfig, data string) (chan bool, error) {
  consumerStopC := make(chan bool)
  return consumerStopC, amqp.StartConsumer(connection, cfg, func(message amqp.Message) {
    // process the message
  }, func() {
    // do something after the process is finished if necessary
  }, consumerStopC)
}
```